package dmysql

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

// DB 数据库处理核心对象
var DB *gorm.DB

// Init 初始化数据库处理
// dsn - 连接字符串
// debug - 是否调试模式
func Init(dsn string, debug bool) {
	nLog := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags),
		logger.Config{
			SlowThreshold:             time.Second,
			LogLevel:                  logger.Silent,
			IgnoreRecordNotFoundError: false,
			ParameterizedQueries:      false,
			Colorful:                  true,
		})
	config := mysql.Config{
		DSN: dsn,
	}
	var lcfg = &gorm.Config{}
	if debug {
		lcfg.Logger = nLog
	}
	db, err := gorm.Open(mysql.New(config), lcfg)
	if err != nil {
		log.Fatalf("数据库连接错误:%v", err)
	}
	pool, poolErr := db.DB()
	if poolErr != nil {
		log.Fatalf("连接池创建失败:%v", err)
	}
	pool.SetMaxIdleConns(10)
	pool.SetMaxOpenConns(100)
	DB = db
	if debug {
		log.Printf("数据库初始化完成:%s", dsn)
	}
}

// ClosePool 手动关闭连接池（一般不需要）
func ClosePool() {
	pool, err1 := DB.DB()
	if err1 != nil {
		log.Fatalf("连接池关闭失败:%v", err1)
		return
	}
	err := pool.Close()
	if err != nil {
		log.Fatalf("连接池关闭失败:%v", err)
		return
	}
}
