package drds

import (
	"github.com/go-redis/redis"
	"log"
)

var RDC *redis.Client

// InitClient 初始化redis
func InitClient(addr string, pwd string, db int) {
	RDC = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pwd,
		DB:       db,
	})
	_, err := RDC.Ping().Result()
	if err != nil {
		log.Fatalf("连接redis发生错误:%v\n", err)
		return
	}
}
