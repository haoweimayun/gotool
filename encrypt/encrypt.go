package encrypt

import (
	"crypto"
	"crypto/md5"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"encoding/pem"
	"fmt"
)

// Md5Str md5字符串加密
func Md5Str(str string) string {
	hasher := md5.New()
	hasher.Write([]byte(str))
	hashed := hasher.Sum(nil)
	md5String := hex.EncodeToString(hashed)

	return md5String
}

// Sha256Str sha256字符串加密
func Sha256Str(str string) string {
	hasher := sha256.New()
	hasher.Write([]byte(str))
	hashed := hasher.Sum(nil)
	encodeStr := hex.EncodeToString(hashed)
	return encodeStr
}

// Sha256StrWithSalt sha256带盐值加密字符串
func Sha256StrWithSalt(str string, salt string) string {
	oStr := str + salt
	return Sha256Str(oStr)
}

// GetRsaPkPair 获取rsa公钥私钥对
// 返回 私钥 公钥
func GetRsaPkPair() (string, string, error) {
	key, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return "", "", err
	}
	privDer := x509.MarshalPKCS1PrivateKey(key)
	privBlock := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: privDer,
	}
	privPem := pem.EncodeToMemory(privBlock)

	publicKey, err := x509.MarshalPKIXPublicKey(&key.PublicKey)
	if err != nil {
		return "", "", err
	}
	pubBlock := &pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: publicKey,
	}

	pubPem := pem.EncodeToMemory(pubBlock)

	return string(privPem), string(pubPem), nil
}

func parsePublicKey(pubKeyPem string) (*rsa.PublicKey, error) {
	block, _ := pem.Decode([]byte(pubKeyPem))
	if block == nil {
		return nil, fmt.Errorf("解析公钥失败")
	}
	publicKey, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	switch pub := publicKey.(type) {
	case *rsa.PublicKey:
		return pub, nil
	default:
		return nil, fmt.Errorf("未知公钥类型")
	}
}

func parsePrivateKey(priKeyPem string) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode([]byte(priKeyPem))
	if block == nil {
		return nil, fmt.Errorf("解析私钥失败")
	}
	privateKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	return privateKey, nil
}

// RsaEnSha256Base64 rsa + sha256加密 返回base64字符串
func RsaEnSha256Base64(str string, publicKeyPem string) string {
	publicKey, err := parsePublicKey(publicKeyPem)
	if err != nil {
		fmt.Println("公钥解析出错：" + err.Error())
		return ""
	}
	encrypted, err := rsa.EncryptOAEP(sha256.New(), rand.Reader, publicKey, []byte(str), []byte(""))
	if err != nil {
		fmt.Println("rsa加密错误：" + err.Error())
		return ""
	}
	return base64.StdEncoding.EncodeToString(encrypted)
}

// RsaDeSha256Base64 rsa + sha256 解密 返回base64字符串
func RsaDeSha256Base64(str string, privateKeyPem string) string {
	decodeString, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		fmt.Println("base64解析失败：" + err.Error())
		return ""
	}
	privateKey, err := parsePrivateKey(privateKeyPem)
	if err != nil {
		fmt.Println("私钥解析出错：" + err.Error())
		return ""
	}
	decrypted, err := rsa.DecryptOAEP(sha256.New(), rand.Reader, privateKey, decodeString, []byte(""))
	if err != nil {
		fmt.Println("rsa解密错误：" + err.Error())
		return ""
	}
	return string(decrypted)
}

// RsaSignSha256Base64 rsa + sha256签名
// 返回 sign(base64)值,hash值
func RsaSignSha256Base64(str string, priKeyPem string) (string, string) {
	hasher := sha256.New()
	hasher.Write([]byte(str))
	digest := hasher.Sum(nil)
	privateKey, err := parsePrivateKey(priKeyPem)
	if err != nil {
		fmt.Println("私钥解析出错：" + err.Error())
		return "", ""
	}
	sign, err := rsa.SignPKCS1v15(rand.Reader, privateKey, crypto.SHA256, digest)
	if err != nil {
		fmt.Println("签名生成出错：" + err.Error())
		return "", ""
	}
	return base64.StdEncoding.EncodeToString(sign), string(digest)
}

// RsaVerifySha256Base64 rsa + sha256验证签名
/**
参数 签名base64 原始字符串 公钥
返回 是否成功验证
*/
func RsaVerifySha256Base64(sign string, str string, pubKeyPem string) bool {
	decodeString, err := base64.StdEncoding.DecodeString(sign)
	if err != nil {
		fmt.Println("base64解析失败：" + err.Error())
		return false
	}
	publicKey, err := parsePublicKey(pubKeyPem)
	if err != nil {
		fmt.Println("公钥解析出错：" + err.Error())
		return false
	}
	hasher := sha256.New()
	hasher.Write([]byte(str))
	// 重新计算hash
	digest := hasher.Sum(nil)
	err = rsa.VerifyPKCS1v15(publicKey, crypto.SHA256, digest, decodeString)
	if err != nil {
		fmt.Println("验证失败：" + err.Error())
		return false
	}
	return true
}
