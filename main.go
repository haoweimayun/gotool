package main

import (
	"fmt"
	"gitee.com/haoweimayun/gotool/encrypt"
	"gitee.com/haoweimayun/gotool/token_jwt"
	"strconv"
)

func main() {
	testJwt()
}

func testJwt() {
	token, err := token_jwt.CreateToken("1", "admin")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Println("token:" + token)
	validToken := token_jwt.ValidToken(token)
	fmt.Println("verify result:" + strconv.FormatBool(validToken))
	parseToken, err := token_jwt.ParseToken(token)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Println("username:" + parseToken.Username)
	fmt.Println("id:" + parseToken.UserId)
}

func testRsa() {
	pri, pub, err := encrypt.GetRsaPkPair()
	if err != nil {
		return
	}
	fmt.Println("public key :" + pub)
	fmt.Println("private key :" + pri)

	enSha256Base64 := encrypt.RsaEnSha256Base64("123456", pub)
	fmt.Println("encode:" + enSha256Base64)

	deSha256Base64 := encrypt.RsaDeSha256Base64(enSha256Base64, pri)
	fmt.Println("decode:" + deSha256Base64)

	sign, _ := encrypt.RsaSignSha256Base64("123456", pri)
	fmt.Println("sign:" + sign)
	verify := encrypt.RsaVerifySha256Base64(sign, "123456", pub)
	fmt.Println("verify:" + strconv.FormatBool(verify))
}
