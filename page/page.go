package page

import "gorm.io/gorm"

// CommonPage 分页通用对象
type CommonPage struct {
	CurrentPage int `json:"currentPage"`
	PageSize    int `json:"pageSize"`
}

// RPage 分页响应对象
type RPage[T any] struct {
	CurrentPage int `json:"currentPage"`
	PageSize    int `json:"pageSize"`
	Total       int `json:"total"`
	Content     []T `json:"content"`
}

// BuildRPage 构建RPage
func BuildRPage[T any](page CommonPage, list []T, total int) RPage[T] {
	return RPage[T]{
		CurrentPage: page.CurrentPage,
		PageSize:    page.PageSize,
		Content:     list,
		Total:       total,
	}
}

// ToDbScope 转换为gorm通用逻辑
func ToDbScope(page CommonPage) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		var cur = page.CurrentPage
		var size = page.PageSize
		offset := (cur - 1) * size
		return db.Offset(offset).Limit(size)
	}
}
