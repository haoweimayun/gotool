package param_valid

import (
	"errors"
	"gitee.com/haoweimayun/gotool/result"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

// ParamVError 校验错误
type ParamVError struct {
	// 属性名
	Prop string `json:"prop"`
	// 错误
	Err string `json:"err"`
}

// ParseJsonBody 解析json body请求体
func ParseJsonBody(ctx *gin.Context, bodyStruct any) (bool, result.R) {
	if err := ctx.ShouldBindJSON(bodyStruct); err != nil {
		var validationErrors validator.ValidationErrors
		if !errors.As(err, &validationErrors) {
			// 不是ValidationErrors类型，返回默认的错误
			r := result.ParamFailS(err.Error())
			ctx.JSON(result.ParamFail(err.Error()))
			return false, r
		}
		// 提取ValidationErrors并转换为ValidationError数组
		var verrs []ParamVError
		for _, err := range err.(validator.ValidationErrors) {
			verr := ParamVError{
				Prop: err.Field(),
				Err:  err.Error(),
			}
			verrs = append(verrs, verr)
		}
		r := result.ParamFailS(verrs)
		ctx.JSON(result.ParamFail(err.Error()))
		return false, r
	}
	return true, result.R{}
}

// ParseFormBody 解析from body请求体
func ParseFormBody(ctx *gin.Context, bodyStruct any) (bool, result.R) {
	if err := ctx.ShouldBind(bodyStruct); err != nil {
		var validationErrors validator.ValidationErrors
		if !errors.As(err, &validationErrors) {
			// 不是ValidationErrors类型，返回默认的错误
			r := result.ParamFailS(err.Error())
			ctx.JSON(result.ParamFail(err.Error()))
			return false, r
		}
		// 提取ValidationErrors并转换为ValidationError数组
		var verrs []ParamVError
		for _, err := range err.(validator.ValidationErrors) {
			verr := ParamVError{
				Prop: err.Field(),
				Err:  err.Error(),
			}
			verrs = append(verrs, verr)
		}
		r := result.ParamFailS(verrs)
		ctx.JSON(result.ParamFail(err.Error()))
		return false, r
	}
	return true, result.R{}
}

// ParseXmlBody 解析xml body请求体
func ParseXmlBody(ctx *gin.Context, bodyStruct any) (bool, result.R) {
	if err := ctx.ShouldBindXML(bodyStruct); err != nil {
		var validationErrors validator.ValidationErrors
		if !errors.As(err, &validationErrors) {
			// 不是ValidationErrors类型，返回默认的错误
			r := result.ParamFailS(err.Error())
			ctx.JSON(result.ParamFail(err.Error()))
			return false, r
		}
		// 提取ValidationErrors并转换为ValidationError数组
		var verrs []ParamVError
		for _, err := range err.(validator.ValidationErrors) {
			verr := ParamVError{
				Prop: err.Field(),
				Err:  err.Error(),
			}
			verrs = append(verrs, verr)
		}
		r := result.ParamFailS(verrs)
		ctx.JSON(result.ParamFail(err.Error()))
		return false, r
	}
	return true, result.R{}
}

// ParseQueryBody 解析query到对象
func ParseQueryBody(ctx *gin.Context, bodyStruct any) (bool, result.R) {
	if err := ctx.ShouldBindQuery(bodyStruct); err != nil {
		var validationErrors validator.ValidationErrors
		if !errors.As(err, &validationErrors) {
			// 不是ValidationErrors类型，返回默认的错误
			r := result.ParamFailS(err.Error())
			ctx.JSON(result.ParamFail(err.Error()))
			return false, r
		}
		// 提取ValidationErrors并转换为ValidationError数组
		var verrs []ParamVError
		for _, err := range err.(validator.ValidationErrors) {
			verr := ParamVError{
				Prop: err.Field(),
				Err:  err.Error(),
			}
			verrs = append(verrs, verr)
		}
		r := result.ParamFailS(verrs)
		ctx.JSON(result.ParamFail(err.Error()))
		return false, r
	}
	return true, result.R{}
}
