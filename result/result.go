package result

// CodeInfo 结果码信息
type CodeInfo struct {
	code  int
	msg   string
	cnMsg string
}

func getResultCode(code int) CodeInfo {
	switch code {
	case SUCCESS:
		{
			return CodeInfo{
				code, "SUCCESS", "请求成功",
			}
		}
	case FAILURE:
		{
			return CodeInfo{
				code, "FAILURE", "请求失败",
			}
		}
	case PARAM_ERR:
		{
			return CodeInfo{
				code, "PARAM_ERR", "参数错误",
			}
		}
	case AUTH_ERR:
		{
			return CodeInfo{
				code, "AUTH_ERR", "认证错误",
			}
		}
	case REMOTE_ERR:
		{
			return CodeInfo{
				code, "REMOTE_ERR", "远程调用错误",
			}
		}
	case METHOD_ERR:
		{
			return CodeInfo{
				code, "METHOD_ERR", "请求方法不支持",
			}
		}
	case SERVER_ERR:
		{
			return CodeInfo{
				code, "SERVER_ERR", "服务器错误",
			}
		}
	case WARN:
		{
			return CodeInfo{
				code, "WARN", "警告",
			}
		}
	default:
		{
			return CodeInfo{
				code: code, msg: "unknown", cnMsg: "未知",
			}
		}

	}
}

const (
	// SUCCESS 成功
	SUCCESS = 0
	// FAILURE 请求失败
	FAILURE = -1
	// PARAM_ERR 参数错误
	PARAM_ERR = -2
	// AUTH_ERR 认证错误
	AUTH_ERR = -3
	// REMOTE_ERR 远程调用错误
	REMOTE_ERR = -4
	// METHOD_ERR 请求方法不支持
	METHOD_ERR = -5
	// SERVER_ERR 服务器错误
	SERVER_ERR = -10
	// WARN 警告
	WARN = 1
	// NOT_FOUND 资源未找到
	NOT_FOUND = 404
)

// R 通用返回结果 /**
type R struct {
	// 返回码
	Code int `json:"code"`
	// 返回信息
	Message string `json:"message"`
	// 数据
	Data any `json:"data"`
}

// Ok 成功
func Ok(data any) (int, R) {
	var codeInfo = getResultCode(SUCCESS)
	r := R{
		Code:    codeInfo.code,
		Message: codeInfo.msg,
		Data:    data,
	}
	return 200, r
}

// OkS 成功
func OkS(data any) R {
	var codeInfo = getResultCode(SUCCESS)
	return R{
		Code:    codeInfo.code,
		Message: codeInfo.msg,
		Data:    data,
	}
}

// Warn 警告
func Warn(msg string) (int, R) {
	var codeInfo = getResultCode(WARN)
	r := R{
		Code:    codeInfo.code,
		Message: msg,
		Data:    nil,
	}
	return 200, r
}

// WarnS 警告
func WarnS(msg string) R {
	var codeInfo = getResultCode(WARN)
	return R{
		Code:    codeInfo.code,
		Message: msg,
		Data:    nil,
	}
}

// ServerFail 服务器错误
func ServerFail(data any) (int, R) {
	var codeInfo = getResultCode(SERVER_ERR)
	r := R{
		Code:    codeInfo.code,
		Message: codeInfo.msg,
	}
	return 500, r
}

// ServerFailS 服务器错误
func ServerFailS(data any) R {
	var codeInfo = getResultCode(SERVER_ERR)
	return R{
		Code:    codeInfo.code,
		Message: codeInfo.msg,
		Data:    data,
	}
}

// ParamFail 参数错误
func ParamFail(data any) (int, R) {
	info := getResultCode(PARAM_ERR)
	r := R{
		Code:    info.code,
		Message: info.msg,
		Data:    data,
	}
	return 400, r
}

// ParamFailS 参数错误
func ParamFailS(data any) R {
	info := getResultCode(PARAM_ERR)
	return R{
		Code:    info.code,
		Message: info.msg,
		Data:    data,
	}
}

// R2Resp r转换为json响应需要的格式
func R2Resp(r *R) (int, R) {
	switch r.Code {
	case SUCCESS:
		return Ok(r.Data)
	case WARN:
		return Warn(r.Message)
	case PARAM_ERR:
		return ParamFail(r.Data)
	case SERVER_ERR:
		return ServerFail(r.Data)
	default:
		return Ok(r.Data)
	}
}
