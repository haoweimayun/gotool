package route

import (
	"gitee.com/haoweimayun/gotool/result"
	"github.com/gin-gonic/gin"
	"net/http"
)

// Route 路由
type Route struct {
	// 请求路径
	Path string
	// 处理方法
	Handler func(context *gin.Context)
	// 请求方式
	Method string
}

// NewRoute 新增路由
func NewRoute(path string, handler func(context *gin.Context), method string) Route {
	return Route{Path: path, Handler: handler, Method: method}
}

const (
	GET    = "GET"
	POST   = "POST"
	PUT    = "PUT"
	DELETE = "DELETE"
	OPTION = "OPTION"
	HEAD   = "HEAD"
	PATCH  = "PATCH"
)

// BaseUrl 项目基础访问路径
var BaseUrl string

// InitBase 初始化项目基础访问路径
func InitBase(base string) {
	BaseUrl = base
}

// InitNoRoute 初始化404
func InitNoRoute(engine *gin.Engine) {
	engine.NoRoute(func(context *gin.Context) {
		uri := context.Request.RequestURI
		context.JSON(http.StatusNotFound, result.R{
			Code:    result.NOT_FOUND,
			Message: "404 NOT FOUND",
			Data:    uri,
		})
	})
}

// AddRouteGroup 添加路由组
// groupPath 路由组访问路径
// routes 路由列表
func AddRouteGroup(groupPath string, engine *gin.Engine, routes *[]Route) {
	baseUrl := BaseUrl
	group := engine.Group(baseUrl + "/" + groupPath)
	for _, r := range *routes {
		switch r.Method {
		case GET:
			group.GET(r.Path, r.Handler)
		case POST:
			group.POST(r.Path, r.Handler)
		case PUT:
			group.PUT(r.Path, r.Handler)
		case HEAD:
			group.HEAD(r.Path, r.Handler)
		case DELETE:
			group.DELETE(r.Path, r.Handler)
		case OPTION:
			group.OPTIONS(r.Path, r.Handler)
		case PATCH:
			group.PATCH(r.Path, r.Handler)
		}
	}
}
