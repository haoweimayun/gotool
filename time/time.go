package time

import (
	"fmt"
	t "time"
)

// GetNowTimeStamp 获取当前时间戳
func GetNowTimeStamp() string {
	now := t.Now().UnixMilli()
	return fmt.Sprintf("%d", now)
}
