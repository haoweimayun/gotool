package token_jwt

import (
	"fmt"
	"github.com/golang-jwt/jwt/v5"
	"time"
)

// UserClaims 用户相关信息
type UserClaims struct {
	jwt.RegisteredClaims
	// 用户登录名
	Username string `json:"username"`
	// 用户id
	UserId string `json:"userId"`
}

// TokenBaseConfig token基础设置
type TokenBaseConfig struct {
	// 过期时间（小时）
	ExpireHours int64 `json:"expireHours"`
	// 签名所需密钥
	SignKey string `json:"signKey"`
}

var DefaultBaseConfig = TokenBaseConfig{
	ExpireHours: 48,
	SignKey:     "jwt_token",
}

var BaseConfig = DefaultBaseConfig

// InitBaseConfig 初始化token相关配置
func InitBaseConfig(expireHours int64, signKey string) {
	BaseConfig = TokenBaseConfig{
		ExpireHours: expireHours,
		SignKey:     signKey,
	}
}

// CreateToken 创建token字符串
func CreateToken(id string, username string) (string, error) {
	claims := UserClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * time.Duration(BaseConfig.ExpireHours))),
		},
		UserId:   id,
		Username: username,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedString, err := token.SignedString([]byte(BaseConfig.SignKey))
	if err != nil {
		fmt.Println("创建token发生错误：" + err.Error())
		return "", err
	}
	return signedString, nil
}

// ValidToken 验证token有效性
func ValidToken(tokenStr string) bool {
	_, err := jwt.ParseWithClaims(tokenStr, &UserClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(BaseConfig.SignKey), nil
	})
	if err != nil {
		fmt.Println("验证token发生错误：" + err.Error())
		return false
	}
	return err == nil
}

// ParseToken 解析token中的数据
func ParseToken(tokenStr string) (*UserClaims, error) {
	token, err := jwt.ParseWithClaims(tokenStr, &UserClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(BaseConfig.SignKey), nil
	})
	if err != nil {
		return nil, err
	}
	if claims, ok := token.Claims.(*UserClaims); ok {
		return claims, nil
	} else {
		return nil, fmt.Errorf("无法处理的负载类型")
	}
}
